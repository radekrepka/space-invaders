#include <corecrt_math_defines.h>
#include <math.h>
#include <vector>
#include "lib/engine.h"
#include "lib/sound.h"
#include "managers/GameManager.h"
#include "gameObjects/GameObject.h"
#include "gameObjects/GameText.h"
#include "gameObjects/Player.h"
#include "gameObjects/Enemy.h"

using namespace std;

#define ENEMY_COUNT 50

void Game()
{
	int musicChannel;

	// Set loading cursor
	SetCursor(LoadCursor(NULL, IDC_WAIT));

	//GameManager init
	GameManager gameManager = GameManager(ENEMY_COUNT);

	//ColliderStore init
	vector<GameObject*> ColliderStore;

	// Player init
	Player Player;
	Player.AddColider(ColliderStore);

	// Start music
	musicChannel = PlayMusic("audio/M.O.O.N - Dust.mp3", 0.3F);

	// Enemies init
	Enemy Enemies[ENEMY_COUNT];
	for (int i = 0; i < ENEMY_COUNT; i++)
	{
		float width = (6 + ((i) % 15));
		float height = (6 + ((i) % 15));
		Enemies[i] = Enemy();
		Enemies[i].Spawn(i, Transform((i % 10) * 60 + 120, (i / 10) * 60 + 70, width, height, 0));
		Enemies[i].AddColider(ColliderStore, Transform(2, 2, width - 5, height - 5, 0));
	}

	// Set normal cursor
	SetCursor(LoadCursor(NULL, IDC_ARROW));

	while (!WantQuit() && !IsKeyDown(VK_ESCAPE))
	{
		gameManager.Update();

		if (!gameManager.isLoosing && !gameManager.isWinning) {
			Player.Update(gameManager, ColliderStore);

			for (int n = 0; n < ENEMY_COUNT; ++n)
			{
				Enemies[n].Update(gameManager, ColliderStore);
			}
		}
		else
		{
			StopMusic();
		}

		Flip();
	}

	return;
}
