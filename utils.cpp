#pragma warning(disable:4995)

#include "utils.h"
#include <iostream>
#include <string>

//-----------------------------------------------------------------------------
// Name: IsCollision()
// Desc: Return true if active gameobjects with colliders are in collision
//-----------------------------------------------------------------------------
bool IsCollision(GameObject &a, GameObject &b)
{
	if (
		a.IsActive() && b.IsActive() &&
		a.tag != b.tag &&
		a.HasCollider() &&
		b.HasCollider() &&
		a.collider.GetNormalX() + a.collider.GetNormalWidth() >= b.collider.GetNormalX() &&
		b.collider.GetNormalX() + b.collider.GetNormalWidth() >= a.collider.GetNormalX() &&
		a.collider.GetNormalY() + a.collider.GetNormalHeight() >= b.collider.GetNormalY() &&
		b.collider.GetNormalY() + b.collider.GetNormalHeight() >= a.collider.GetNormalY()
		) {
		return true;
	}

	return false;
}