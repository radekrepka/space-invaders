#pragma once

#include "gameObjects/GameObject.h"

#ifndef __utils_H__
#define __utils_H__

bool IsCollision(GameObject &a, GameObject &b);

#endif
