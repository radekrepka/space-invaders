#pragma once

#ifndef __GUIManager_H__
#define __GUIManager_H__

#include "GameManager.h"
#include "../gameObjects/GameText.h"

#define PAUSE_X 50
#define PAUSE_Y 50
#define PAUSE_W 50
#define PAUSE_H 50

class GUIManager
{
	public:
		GUIManager();
		void Update(GameManager &gameManager);

	private:
		void GUIManager::MusicTextAnimation(GameText &MusicNameText, GameText &MusicText, GameManager &gameManager);
};
#endif
