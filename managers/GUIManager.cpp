#pragma warning(disable:4995)

#include "GUIManager.h"
#include "../gameObjects/GameText.h"

GUIManager::GUIManager()
{
	
}

//-----------------------------------------------------------------------------
// Name: GUIManager::Update()
// Desc: Update all GUI components, in the first call init GUI components
//-----------------------------------------------------------------------------
void GUIManager::Update(GameManager &gameManager)
{
	// Load GUI sprites
	static void *PauseSprite = LoadSprite("gfx/pause.png");
	static void *PlaySprite = LoadSprite("gfx/play.png");

	static GameText MusicText = GameText("music");
	static GameText MusicNameText = GameText("m.o.o.n - dust");

	static GameText TitleText = GameText("space invaders");

	static GameText ScoreText = GameText("score");
	static GameText ScoreValueText = GameText("0");

	static GameText HighScoreText = GameText("high score");
	static GameText HighScoreValueText = GameText(to_string(gameManager.highScore));

	static void *GameOverSprite = LoadSprite("gfx/game_over.png");
	static void *VictorySprite = LoadSprite("gfx/victory.png");

	if (gameManager.isWinning)
	{
		// Draw victory screen
		DrawSprite(VictorySprite, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 150, 50, 0, 0xffffffff);
	}
	else if (gameManager.isLoosing)
	{
		// Draw game over screen
		DrawSprite(GameOverSprite, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 150, 50, 0, 0xffffffff);
	}
	else
	{
		// Draw play/pause button
		if (gameManager.IsPause())
		{
			DrawSprite(PlaySprite, PAUSE_X, PAUSE_Y, PAUSE_W, PAUSE_H, 0, 0xffffffff);
		}
		else
		{
			DrawSprite(PauseSprite, PAUSE_X, PAUSE_Y, PAUSE_W, PAUSE_H, 0, 0xffffffff);
		}

		// Draw title text
		TitleText.Draw(gameManager.time, 40, 126, 30, 20, 20);

		// Draw score
		ScoreText.Draw(gameManager.time, 20, SCREEN_WIDTH - 100, 30, 10, 10);
		ScoreValueText.SetText(to_string(gameManager.score));
		ScoreValueText.Draw(gameManager.time, 20, SCREEN_WIDTH - 60, 55, 10, 10);

		// Draw high score
		HighScoreText.Draw(gameManager.time, 12, SCREEN_WIDTH - 120, 90, 5, 9);
		HighScoreValueText.SetText(to_string(gameManager.highScore));
		HighScoreValueText.Draw(gameManager.time, 15, SCREEN_WIDTH - 60, 115, 10, 10);

		// Music text animation
		MusicTextAnimation(MusicNameText, MusicText, gameManager);
	}
}

//-----------------------------------------------------------------------------
// Name: GUIManager::MusicTextAnimation()
// Desc: Show and hide music name
//-----------------------------------------------------------------------------
void GUIManager::MusicTextAnimation(GameText &MusicNameText, GameText &MusicText, GameManager &gameManager)
{
	static int transparency = 0;
	static bool back = false;

	if (gameManager.time % 2 == 0)
	{
		if (!back) {
			transparency++;
			back = transparency == 0xee;
		}
		else
		{
			transparency = transparency > 0 ? transparency - 1 : 0;
		}
	}

	if (transparency > 0) {
		MusicText.Draw(gameManager.time, 20, 20, SCREEN_HEIGHT - 60, 10, 10, (transparency * 0xffffff + transparency) + 0x00ffffff);
		MusicNameText.Draw(gameManager.time, 20, 20, SCREEN_HEIGHT - 30, 10, 10, (transparency * 0xffffff + transparency) + 0x00ffffff);
	}
}
