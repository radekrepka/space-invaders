#pragma once

#ifndef __GameManager_H__
#define __GameManager_H__

#include <vector>

#define PAUSE_X 50
#define PAUSE_Y 50
#define PAUSE_W 50
#define PAUSE_H 50

#define HIGH_SCORE_FILE "high_score.dat"

class GUIManager;
class GameObject;

class GameManager
{
	public:
		GameManager(int maxScore);
		int time;
		int timeSpeed;
		int score;
		int highScore;
		int maxScore;
		bool isLoosing;
		bool isWinning;
		void Update();
		void SetPause(bool pause);
		void GameOver();
		void TogglePause();
		bool IsPause();

	private:
		GUIManager *guiManager;
		void GameManager::Pause();
		void *GameOverSound;
		void *TadaSound;
		void SetHighScore(int score);
		int LoadHighScore();
};
#endif
