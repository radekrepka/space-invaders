#pragma warning(disable:4995)

#include "GameManager.h"
#include <fstream>
#include "GUIManager.h"
#include "../lib/sound.h"
#include "../gameObjects/GameObject.h"

GameManager::GameManager(int maxScore)
{
	this->time = 0;
	this->timeSpeed = 1;
	this->score = 0;
	this->highScore = this->LoadHighScore();
	this->maxScore = maxScore;
	this->isWinning = false;
	this->isLoosing = false;
	this->GameOverSound = LoadSnd("audio/game_over.mp3", false);
	this->TadaSound = LoadSnd("audio/tada.mp3", false);
	guiManager = new GUIManager();
}

void GameManager::Update()
{
	this->time = this->time + this->timeSpeed;

	if (this->score > this->highScore)
	{
		this->SetHighScore(this->score);
	}

	this->Pause();
	this->guiManager->Update(*this);
	
	if (this->score >= 50 && !this->isWinning)
	{
		PlaySnd(TadaSound, 1.0F);
		this->isWinning = true;
	}

	static bool played = false;
	if (this->isLoosing && !played)
	{
		PlaySnd(GameOverSound, 1.0F);
		played = true;
	}
}

void GameManager::SetPause(bool pause)
{
	this->timeSpeed = pause ? 0 : 1;
}

//-----------------------------------------------------------------------------
// Name: GameManager::TogglePause()
// Desc: Change pause status
//-----------------------------------------------------------------------------
void GameManager::TogglePause()
{
	if (this->timeSpeed == 1)
	{
		this->timeSpeed = 0;
	}
	else
	{
		this->timeSpeed = 1;
	}
}

//-----------------------------------------------------------------------------
// Name: GameManager::Pause()
// Desc: Set pause if user click on pause button
//-----------------------------------------------------------------------------
void GameManager::Pause()
{
	float mx, my;
	GetMousePos(mx, my);
	static bool pressed = false;

	if ((mx >= PAUSE_X / 2 && mx <= PAUSE_X / 2 + PAUSE_W) &&
		(my >= PAUSE_Y / 2 && my <= PAUSE_Y / 2 + PAUSE_H)
		) {
		if (IsKeyDown(VK_LBUTTON))
		{
			if (!pressed)
			{
				this->TogglePause();
				pressed = true;
			}
		}
		else
		{
			pressed = false;
			SetCursor(LoadCursor(NULL, IDC_HAND));
		}
	}
	else
	{
		SetCursor(LoadCursor(NULL, IDC_ARROW));
	}
}

void GameManager::GameOver()
{
	this->isLoosing = true;
}

//-----------------------------------------------------------------------------
// Name: GameManager::SetHighScore()
// Desc: Set new high score and save to file
//-----------------------------------------------------------------------------
void GameManager::SetHighScore(int score)
{
	this->highScore = score;
	ofstream file(HIGH_SCORE_FILE);
	if (file.is_open())
	{
		file << score;
		file.close();
	}
}

//-----------------------------------------------------------------------------
// Name: GameManager::LoadHighScore()
// Desc: Load high score from file
//-----------------------------------------------------------------------------
int GameManager::LoadHighScore()
{
	ifstream inFile;
	string line;
	int score = 0;

	inFile.open(HIGH_SCORE_FILE);

	if (inFile && inFile.good()) {
		getline(inFile, line);
		score = atoi(line.c_str());
	}

	inFile.close();

	return score;
}

bool GameManager::IsPause()
{
	return this->timeSpeed == 0;
}
