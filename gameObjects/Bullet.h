#pragma once

#ifndef __Bullet_H__
#define __Bullet_H__

#include "../managers/GameManager.h"
#include "GameObject.h"
#include "Explosion.h"

class Bullet : public GameObject
{
	public:
		Bullet();
		Bullet(void *ExplosionSound);

		void *ExplosionSound;

		void Update(GameManager &gameManager, vector<GameObject*> &ColliderStore);
		void Spawn(float x, float y);

	private:
		void OnCollision(vector<GameObject*> CollisionedGameObjects, GameManager &gameManager, vector<GameObject*> &ColliderStore);
		bool isExploding;
		Explosion *explosion;
};
#endif
