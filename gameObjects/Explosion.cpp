#pragma warning(disable:4995)

#include "Explosion.h"
#include <sstream>

Explosion::Explosion() : GameObject()
{
	this->step = 0;
	this->isAnimationEnd = false;

	// Init explosion sprites
	for (int i = 0; i < EXPLOSION_IMAGE_COUNT; i++) {
		stringstream ss;
		ss << "gfx/explosion/" << i + 1 << ".png";
		this->ExplosionSprites[i] = LoadSprite(ss.str().c_str());
	};
}

//-----------------------------------------------------------------------------
// Name: Explosion::Update()
// Desc: Update explosion animation state
//-----------------------------------------------------------------------------
void Explosion::Update(GameManager &gameManager)
{
	if (this->step < EXPLOSION_IMAGE_COUNT)
	{
		this->Sprite = this->ExplosionSprites[step];
		this->Draw(this->transform);

		if (gameManager.time % 3 == 0) this->step++;
	}
	else
	{
		this->isAnimationEnd = true;
	}
}

bool Explosion::IsAnimationEnd()
{
	return this->isAnimationEnd;
}
