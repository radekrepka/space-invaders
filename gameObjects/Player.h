#pragma once

#ifndef __Player_H__
#define __Player_H__

#include "../managers/GameManager.h"
#include "GameObject.h"
#include "Bullet.h"
#include "Explosion.h"

#define MAX_BULLET_COUNT 10

class Player : public GameObject
{
	public:
		Player();

		void Update(GameManager &gameManager, vector<GameObject*> &ColliderStore);
		void OnCollision(vector<GameObject*> CollisionedGameObjects, GameManager &gameManager, vector<GameObject*> &ColliderStore);
		void Control(GameManager &gameManager);
		void Fire(GameManager &gameManager, vector<GameObject*> &ColliderStore);
		void AddColider(vector<GameObject*> &ColliderStore);

	private:
		void *LaserSound;
		void *ExplosionSound;
		Bullet bullets[MAX_BULLET_COUNT];
		bool isExploding;
		Explosion *explosion;
};
#endif
