#pragma once

#ifndef __Enemy_H__
#define __Enemy_H__

#include "GameObject.h"
#include "components/Transform.h"

class Enemy : public GameObject
{
	public:
		Enemy();
		int index;
		Transform staticTransform;
		void Spawn(int index, Transform transform);
		void Update(GameManager &gameManager, vector<GameObject*> &ColliderStore);

	private:

};
#endif
