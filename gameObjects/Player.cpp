#pragma warning(disable:4995)

#include "Player.h"
#include <math.h>
#include <corecrt_math_defines.h>
#include "../lib/engine.h"
#include "../lib/sound.h"
#include "Bullet.h"

Bullet bullets[MAX_BULLET_COUNT];

Player::Player() : GameObject("gfx/beautiful_spaceship.png")
{
	this->transform.x = SCREEN_WIDTH / 2;
	this->transform.y = SCREEN_HEIGHT - 70;
	this->tag = "Player";
	this->isExploding = false;
	this->LaserSound = LoadSnd("audio/laser.mp3", false);
	this->ExplosionSound = LoadSnd("audio/explosion.mp3", false);
	this->explosion = new Explosion();
}

void Player::Update(GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	GameObject::Update(gameManager, ColliderStore);

	if (!this->isExploding) {
		this->Control(gameManager);
	}
	else
	{
		//After explosing animation show game over screen
		static bool isAnimationEnd = false;

		if (!isAnimationEnd)
		{
			(*this->explosion).Update(gameManager);
			if ((*this->explosion).IsAnimationEnd())
			{
				gameManager.GameOver();
				isAnimationEnd = true;
			}
		}
	}

	// Update bullets
	for (int n = 0; n < MAX_BULLET_COUNT; n++)
	{
		this->bullets[n].Update(gameManager, ColliderStore);
	}

	if (!gameManager.IsPause() && this->isActive)
	{
		this->Fire(gameManager, ColliderStore);
	}
}

//-----------------------------------------------------------------------------
// Name: Player::OnCollision()
// Desc: Called after collision with another gameobject. Destroy player and start explosion in collision with enemy
//-----------------------------------------------------------------------------
void Player::OnCollision(vector<GameObject*> CollisionedGameObjects, GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	for (int i = 0; i < CollisionedGameObjects.size(); i++)
	{
		if (CollisionedGameObjects[i]->tag == "Enemy") {
			PlaySnd(ExplosionSound, 1.0F);
			this->Destroy();
			this->isExploding = true;
			(*this->explosion).Spawn(Transform(
				this->transform.x,
				this->transform.y,
				this->transform.width * 2,
				this->transform.height * 2,
				this->transform.angle
			));
		}
	}
}

void Player::Control(GameManager &gameManager)
{
	int isLeftKeyDown = IsKeyDown(VK_LEFT) || IsKeyDown(0x41);
	int isRightKeyDown = IsKeyDown(VK_RIGHT) || IsKeyDown(0x44);
	int speed = 7 * gameManager.timeSpeed;

	if (isLeftKeyDown) {
		speed = this->transform.x > 50 + 15 ? -speed : 0;
	}
	else if (isRightKeyDown)
	{
		speed = this->transform.x < SCREEN_WIDTH - 50 - 15 ? speed : 0;
	}
	else
	{
		speed = 0;
	}

	this->Draw(this->transform.x += speed, this->transform.y, 50, 50, sin(gameManager.time*0.1)*0.1, 0xffffffff);
}

void Player::Fire(GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	static int b = 0;
	static int count = 0;
	static bool pressed = false;

	if (count) --count;
	if (!IsKeyDown(VK_SPACE)) {
		pressed = false;
	}

	if (IsKeyDown(VK_SPACE) && !pressed && count == 0) {
		PlaySnd(LaserSound, 1.0F);
		this->bullets[b] = Bullet(ExplosionSound);
		this->bullets[b].Spawn(this->transform.x, this->transform.y - 10);
		this->bullets[b].AddColider(ColliderStore, Transform(0, 0, 10, 10, M_PI / 2));
		b = (b + 1) % MAX_BULLET_COUNT;
		count = 50;
		pressed = true;
	}
}

void Player::AddColider(vector<GameObject*> &ColliderStore)
{
	GameObject::AddColider(ColliderStore, Transform(5, 0, 40, 50, 0));
}
