#pragma once

#ifndef __Explosion_H__
#define __Explosion_H__

#include "GameObject.h"

#define EXPLOSION_IMAGE_COUNT 8

class Explosion : public GameObject
{
	public:
		Explosion();
		void Update(GameManager &gameManager);
		bool IsAnimationEnd();

	private:
		void *ExplosionSprites[EXPLOSION_IMAGE_COUNT];
		int step;
		int speed;
		bool isAnimationEnd;
};
#endif