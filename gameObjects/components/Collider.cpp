#pragma warning(disable:4995)

#include "Collider.h"

Collider::Collider()
{

}

Collider::Collider(Transform transform)
{
	this->staticTransform = transform;
}

void Collider::Update(Transform transform)
{
	Transform::Update(
		transform.x + this->staticTransform.x,
		transform.y + this->staticTransform.y,
		this->staticTransform.width,
		this->staticTransform.height,
		transform.angle
	);
}
