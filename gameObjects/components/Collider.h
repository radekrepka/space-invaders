#pragma once

#ifndef __Collider_H__
#define __Collider_H__

#include "Transform.h"

class Collider : public Transform
{
	public:
		Collider();
		Collider(Transform transform);
		Transform staticTransform;

		void Update(Transform transform);

	protected:
};
#endif
