#pragma once

#ifndef __Transform_H__
#define __Transform_H__

class Transform
{
	public:
		Transform();
		Transform(float x, float y, float width, float height, float angle);
		void Update(float x, float y, float width, float height, float angle);

		float x;
		float y;
		float width;
		float height;
		float angle;

		float GetX();
		float GetY();
		float GetWidth();
		float GetHeight();
		float GetAngle();
		float GetNormalX();
		float GetNormalY();
		float GetNormalWidth();
		float GetNormalHeight();

	protected:
};
#endif
