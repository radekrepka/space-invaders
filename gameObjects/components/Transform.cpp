#pragma warning(disable:4995)

#include "Transform.h"

Transform::Transform()
{

}

Transform::Transform(float x, float y, float width, float height, float angle)
{
	this->Update(x, y, width, height, angle);
}

void Transform::Update(float x, float y, float width, float height, float angle)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->angle = angle;
}

float Transform::GetX()
{
	return this->x;
}

float Transform::GetY()
{
	return this->y;
}

float Transform::GetWidth()
{
	return this->width;
}

float Transform::GetHeight()
{
	return this->height;
}

float Transform::GetAngle()
{
	return this->angle;
}

float Transform::GetNormalX()
{
	return this->x - this->width;
}

float Transform::GetNormalY()
{
	return this->y - this->height;
}

float Transform::GetNormalWidth()
{
	return this->width * 2;
}

float Transform::GetNormalHeight()
{
	return this->height * 2;
}
