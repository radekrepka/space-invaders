#pragma warning(disable:4995)

#include "Enemy.h"
#include <corecrt_math_defines.h>

Enemy::Enemy() : GameObject("gfx/enemy.png")
{
	this->tag = "Enemy";
}

//-----------------------------------------------------------------------------
// Name: Enemy::Spawn()
// Desc: Set enemy index and position
//-----------------------------------------------------------------------------
void Enemy::Spawn(int index, Transform transform)
{
	GameObject::Spawn();
	this->index = index;
	this->transform = transform;
	this->staticTransform = transform;
}

//-----------------------------------------------------------------------------
// Name: Enemy::Update()
// Desc: Enemy "AI"
//-----------------------------------------------------------------------------
void Enemy::Update(GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	int xo = 0, yo = 0;
	int n1 = gameManager.time + index * index + index * index*index;
	int n2 = gameManager.time + index + index * index + index * index*index * 3;
	if (((n1 >> 6) & 0x7) == 0x7)xo += (1 - cos((n1 & 0x7f) / 64.0f*2.f*M_PI))*(20 + ((index*index) % 9));
	if (((n1 >> 6) & 0x7) == 0x7)yo += (sin((n1 & 0x7f) / 64.0f*2.f*M_PI))*(20 + ((index*index) % 9));
	if (((n2 >> 8) & 0x5) == 0x5)yo += (1 - cos((n2 & 0xff) / 256.0f*2.f*M_PI))*(200 + ((index*index) % 9));

	this->Draw(this->staticTransform.x + xo, this->staticTransform.y + yo, this->staticTransform.width, this->staticTransform.height, 0, 0xffffffff);
	GameObject::Update(gameManager, ColliderStore);
}
