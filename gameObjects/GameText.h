#pragma once

#include "GameObject.h"
#include <vector>
#include <string>

using namespace std;

#ifndef __GameText_H__
#define __GameText_H__

#define CHARS_DIR "gfx/chars/"
#define SUPPORTED_CHARS_COUNT 38

class GameText : public GameObject
{
	public:
		GameText(string text);

		void Draw(int time, float charSpace, float xCentre, float yCentre, float w, float h, DWORD col = 0xffffffff);
		void SetText(string text);

	private:
		int textLength;
		vector<void *> Text; 
		void *CharSprites[SUPPORTED_CHARS_COUNT];
};
#endif
