#pragma warning(disable:4995)

#include "Bullet.h"
#include <corecrt_math_defines.h>
#include "../lib/sound.h"

Bullet::Bullet() : GameObject("gfx/bullet.png")
{
	this->tag = "Bullet";
	this->isExploding = false;
}

Bullet::Bullet(void *ExplosionSound) : GameObject("gfx/bullet.png")
{
	this->tag = "Bullet";
	this->isExploding = false;
	this->explosion = new Explosion();
	this->ExplosionSound = ExplosionSound;
}

//-----------------------------------------------------------------------------
// Name: Bullet::Spawn()
// Desc: Set bullet position
//-----------------------------------------------------------------------------
void Bullet::Spawn(float x, float y)
{
	GameObject::Spawn();
	this->transform.x = x;
	this->transform.y = y;
}

//-----------------------------------------------------------------------------
// Name: Bullet::Update()
// Desc: Update bullet position. Update explosion animation in explosion case.
//-----------------------------------------------------------------------------
void Bullet::Update(GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	GameObject::Update(gameManager, ColliderStore);

	if (!this->isExploding) {
		int speed = !gameManager.IsPause() ? 4 : 0;
		this->Draw(transform.x, transform.y -= speed, 10, 10, M_PI / 2, 0xffffffff);
	}
	else
	{
		(*this->explosion).Update(gameManager);
	}
}

//-----------------------------------------------------------------------------
// Name: Bullet::Oncollision()
// Desc: In collision case, destroy bullet and enemy and start explosion animation and sound effect
//-----------------------------------------------------------------------------
void Bullet::OnCollision(vector<GameObject*> CollisionedGameObjects, GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	for (int i = 0; i < CollisionedGameObjects.size(); i++)
	{
		if (CollisionedGameObjects[i]->tag == "Enemy") {
			gameManager.score += 1;
			CollisionedGameObjects[i]->Destroy();
			this->Destroy();
			PlaySnd(this->ExplosionSound, 1.0F);
			this->isExploding = true;
			(*this->explosion).Spawn(Transform(
				this->transform.x,
				this->transform.y,
				this->transform.width * 2,
				this->transform.height * 2,
				this->transform.angle
			));
		}
	}
}
