#pragma warning(disable:4995)

#include "Gameobject.h"
#include "../lib/engine.h"
#include "../utils.h"


GameObject::GameObject()
{
	
}

GameObject::GameObject(const char *spritePath)
{
	this->Sprite = LoadSprite(spritePath);
}

GameObject::GameObject(void *Sprite)
{
	this->Sprite = Sprite;
}

void GameObject::Spawn()
{
	this->isActive = true;
}

void GameObject::Spawn(Transform transform)
{
	this->Spawn();
	this->transform = transform;
}

//-----------------------------------------------------------------------------
// Name: GameObject::Draw()
// Desc: Draw gameobject and update gameobject transform
//-----------------------------------------------------------------------------
void GameObject::Draw(float x, float y, float width, float height, float angle, DWORD col)
{
	if (isActive) {
		this->transform.Update(x, y, width, height, angle);

		if (this->hasCollider)
		{
			this->collider.Update(this->transform);
		}

		DrawSprite(Sprite, x, y, width, height, angle, col);
	}
}

void GameObject::Draw(Transform transfrom)
{
	this->Draw(transform.x, transform.y, transform.width, transform.height, transform.angle);
}

void GameObject::Destroy()
{
	isActive = false;
}

//-----------------------------------------------------------------------------
// Name: GameObject::AddColider()
// Desc: Set gameobject collider with default transform and add to collider store
//-----------------------------------------------------------------------------
void GameObject::AddColider(vector<GameObject*> &ColliderStore)
{
	this->AddColider(ColliderStore, Transform(0, 0, this->transform.width, this->transform.height, this->transform.angle));
}

//-----------------------------------------------------------------------------
// Name: GameObject::AddColider()
// Desc: Set gameobject collider and add to collider store
//-----------------------------------------------------------------------------
void GameObject::AddColider(vector<GameObject*> &ColliderStore, Transform transform)
{
	if (!this->hasCollider) {
		this->hasCollider = true;
		this->collider = Collider(transform);
		ColliderStore.push_back(this);
	}
}

//-----------------------------------------------------------------------------
// Name: GameObject::Update()
// Desc: Update gameobject (check collisions)
//-----------------------------------------------------------------------------
void GameObject::Update(GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	if (!gameManager.IsPause()) {
		if (this->hasCollider)
		{
			this->collider.Update(this->transform);
			vector<GameObject*> CollisionedGameObjects = this->DetectCollisions(ColliderStore);

			if (CollisionedGameObjects.size() > 0) {
				this->OnCollision(CollisionedGameObjects, gameManager, ColliderStore);
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Name: GameObject::DetectCollisions()
// Desc: Return all collisioned gameobjets
//-----------------------------------------------------------------------------
vector<GameObject*> GameObject::DetectCollisions(vector<GameObject*> &ColliderStore)
{
	vector<GameObject*> CollisionedGameObjects;
	for(int i = 0; i < ColliderStore.size(); i++)
	{
		if (IsCollision(*ColliderStore[i], *this)) 
		{
			CollisionedGameObjects.push_back(ColliderStore[i]);
		}
	}

	return CollisionedGameObjects;
}

//-----------------------------------------------------------------------------
// Name: GameObject::OnCollision()
// Desc: Called after collision with another gameobject
//-----------------------------------------------------------------------------
void GameObject::OnCollision(vector<GameObject*> CollisionedGameObjects, GameManager &gameManager, vector<GameObject*> &ColliderStore)
{
	
}

bool GameObject::IsActive()
{
	return this->isActive;
}

bool GameObject::HasCollider()
{
	return this->hasCollider;
}
