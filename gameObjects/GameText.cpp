#pragma warning(disable:4995)

#include "GameText.h"
#include <sstream>

using namespace std;

char supportedChars[SUPPORTED_CHARS_COUNT] = {
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
	'0','1','2','3','4','5','6','7','8','9','-','.'
};

GameText::GameText(string text) : GameObject()
{
	// Load character sprites
	stringstream ss;
	for (int i = 0; i < SUPPORTED_CHARS_COUNT - 2; i++) {
		ss << CHARS_DIR << supportedChars[i] << ".png";
		this->CharSprites[i] = LoadSprite(ss.str().c_str());
		ss.str("");
	};
	ss << CHARS_DIR << "dash.png";
	this->CharSprites[SUPPORTED_CHARS_COUNT - 2] = LoadSprite(ss.str().c_str());
	ss.str("");
	ss << CHARS_DIR << "dot.png";
	this->CharSprites[SUPPORTED_CHARS_COUNT - 1] = LoadSprite(ss.str().c_str());
	ss.str("");

	this->SetText(text);
}

void GameText::Draw(int time, float charSpace, float xCentre, float yCentre, float w, float h, DWORD col)
{
	for (int n = 0; n < textLength; ++n) if ((void **)Text[n])
	{
		DrawSprite((void **)Text[n], n * charSpace + xCentre, yCentre, w, h, sin(time * 0.1) * n * 0.01, col);
	}
}

void GameText::SetText(string text)
{
	this->Text.clear();
	this->textLength = text.length();
	for (int i = 0; i < textLength; i++) {
		auto itr = find(supportedChars, end(supportedChars), text[i]);

		if (itr != end(supportedChars)) {
			this->Text.push_back(this->CharSprites[distance(supportedChars, itr)]);
		}
		else
		{
			this->Text.push_back(0);
		}
	}
}
