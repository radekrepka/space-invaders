#pragma once

#ifndef __GameObject_H__
#define __GameObject_H__

#include <vector>
#include <string>
#include "../lib/engine.h"
#include "../managers/GameManager.h"
#include "components/Transform.h"
#include "components/Collider.h"

using namespace std;

class GameObject
{
	public:
		GameObject();
		GameObject(const char *sprite);
		GameObject(void *Sprite);

		string tag;
		void *Sprite;
		Transform transform;
		Collider collider;

		void Spawn();
		void Spawn(Transform transform);
		void Destroy();
		void Draw(float x, float y, float width, float height, float angle, DWORD col = 0xffffffff);
		void Draw(Transform transfrom);
		void AddColider(vector<GameObject*> &ColliderStore);
		void AddColider(vector<GameObject*> &ColliderStore, Transform transform);
		void Update(GameManager &gameManager, vector<GameObject*> &ColliderStore);

		bool IsActive();
		bool HasCollider();

	protected:
		bool isActive;
		bool hasCollider = false;

		vector<GameObject*> DetectCollisions(vector<GameObject*> &ColliderStore);
		virtual void OnCollision(vector<GameObject*> CollisionedGameObjects, GameManager &gameManager, vector<GameObject*> &ColliderStore);
	private:
};
#endif
