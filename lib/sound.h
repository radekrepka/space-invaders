#pragma once

// sound
int PlayMusic(const char *fname, float volume = 1); // returns a handle which you only need if you want to change volume later with ChangeVolume
void PauseMusic(int channel, signed char pause);
void StopMusic();

// sorry for this being 'snd' but otherwise it clashes with a windows #define (PlaySound) grr
void *LoadSnd(const char *fname, bool loop = false); // if you set loop the sample will go on looping forever, until you call StopSound
int PlaySnd(void *sound, float volume = 1); // returns a handle which you only need if you are going to call StopSound or ChangeVolume()
void StopSnd(int handle);
void ChangeVolume(int handle, float newvolume = 1);