#pragma warning(disable:4995)
#include "sound.h"
#include <fmod.h>
#include <d3d9.h>

#pragma comment(lib,"fmodvc.lib")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")


FSOUND_STREAM *music;

int PlayMusic(const char *fname, float volume)
{
	if (music) StopMusic();
	music = FSOUND_Stream_Open(fname, FSOUND_LOOP_NORMAL, 0, 0);
	int chan = FSOUND_Stream_Play(FSOUND_FREE, music);
	if (volume <= 0) volume = 0;
	if (volume > 1) volume = 1;
	FSOUND_SetVolume(chan, (int)(volume * 255));
	return chan;
}

void PauseMusic(int channel, signed char pause) 
{
	if(music)
	{
		FSOUND_SetPaused(channel, pause);
	}
	music = NULL;
}

void StopMusic()
{
	if (music)
	{
		FSOUND_Stream_Close(music);
	}
	music = NULL;
}

void *LoadSnd(const char *fname, bool looped) // TODO Sound store for loaded sounds
{
	int flags = 0;
	if (looped) flags |= FSOUND_LOOP_NORMAL;
	return FSOUND_Sample_Load(FSOUND_FREE, fname, flags, 0, 0);
}

int PlaySnd(void *sound, float volume)
{
	if (!sound) return -1;
	if (volume <= 0) volume = 0;
	if (volume > 1) volume = 1;
	int chan = FSOUND_PlaySound(FSOUND_FREE, (FSOUND_SAMPLE*)sound);
	FSOUND_SetVolume(chan, (int)(volume * 255));
	return chan;
}

void StopSnd(int handle)
{
	if (handle <= 0) return;
	FSOUND_StopSound(handle);
}

void ChangeVolume(int handle, float volume)
{
	if (handle <= 0) return;
	if (volume <= 0) volume = 0;
	if (volume > 1) volume = 1;
	FSOUND_SetVolume(handle, (int)(volume * 255));
}
